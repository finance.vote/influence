import axios from 'axios';
import Block from 'components/Ui/Block';
import CountdownContent from 'components/Ui/CountdownContent';
import Icon from 'components/Ui/Icon';
import { STATUSES } from 'helpers/statuses';
import { ipfsUrl } from 'helpers/utils';
import twitterIcon from 'images/x-icon.png';
import moment from 'moment';
import Image from 'next/image';
import { memo } from 'react';
import Countdown from 'react-countdown';
import styles from './DetailsSection.module.scss';
const dateFormat = 'DD/MM/YYYY HH:mm';

const getDataToCountDown = ({ startDate, endDate, status, upDownEndDate }) => {
  let data = {};
  switch (status) {
    case STATUSES.OPEN:
    case STATUSES.CLOSED:
      data = {
        label: 'Time to end',
        date: endDate
      };
      break;
    case STATUSES.AWAITING:
      data = {
        label: 'Time to start',
        date: startDate
      };
      break;
    case STATUSES.RANKING:
      data = {
        label: 'Time to ranking end',
        date: upDownEndDate
      };
      break;
  }

  return data;
};

const DetailsSection = ({ space, data: { questionData, submissionsData } }) => {
  const { questionHash, startDate, endDate, isOpenTask, status, upDownEndDate } = questionData;
  const countdownData = getDataToCountDown({
    startDate,
    endDate,
    isOpenTask,
    status,
    upDownEndDate
  });
  const generateJSONResult = () => {
    var x = window.open();
    x.document.open();
    x.document.write(
      '<html><body><pre>' +
        JSON.stringify({ ...questionData, submissions: submissionsData }, null, '\t') +
        '</pre></body></html>'
    );
    x.document.close();
  };

  const shareOnXurl = encodeURI(
    `https://twitter.com/intent/tweet?url=${location.origin + location.pathname}`
  );

  return (
    <Block title="Details">
      <div className={styles.container}>
        <div className={styles.item}>
          <span className={styles.label}>IPFS:</span>
          <span className={styles.value}>
            <a
              href={`${ipfsUrl(questionHash)}`}
              className={styles.value}
              target="_blank"
              rel="noopener noreferrer">
              {`#${questionHash?.slice(0, 7)} `}
              <Icon name="external-link" />
            </a>
          </span>
        </div>
        <div className={styles.item}>
          <span className={styles.label}>Start Date:</span>
          <span className={styles.value}>
            <span>{moment(startDate).format(dateFormat)}</span>
          </span>
        </div>
        <div className={styles.item}>
          <span className={styles.label}>End Date:</span>
          <span className={styles.value}>
            <span>{moment(endDate).format(dateFormat)}</span>
          </span>
        </div>
        <div className={styles.item}>
          <span className={styles.label}>Ranking End Date:</span>
          <span className={styles.value}>
            <span>{moment(upDownEndDate ?? endDate).format(dateFormat)}</span>
          </span>
        </div>
        <div className={styles.item}>
          <span className={styles.label}>{countdownData.label}:</span>
          <span className={styles.value}>
            <Countdown date={countdownData.date} renderer={CountdownContent} />
          </span>
        </div>
        <div className={styles.item}>
          <span className={styles.label}>Data:</span>
          <span className={styles.value}>
            <button className={styles.results} onClick={generateJSONResult}>
              JSON <Icon name="external-link" />
            </button>
            /
            <a
              className={styles.results}
              href={`${axios.defaults.baseURL}/api/tasks/${space.key}/${questionHash}/csv`}
              target="_blank"
              rel="noreferrer">
              CSV <Icon name="external-link" />
            </a>
          </span>
        </div>
        <div className={styles.item}>
          <span className={styles.label}>Share on x:</span>
          <span className={styles.value}>
            <button className={styles.twitterButton}>
              <a href={shareOnXurl} target="_blank" rel="noreferrer">
                <div className={styles.twitterIcon}>
                  <Image src={twitterIcon} alt="Twitter" />
                  <span>Post</span>
                </div>
              </a>
            </button>
          </span>
        </div>
      </div>
    </Block>
  );
};

export default memo(DetailsSection);
