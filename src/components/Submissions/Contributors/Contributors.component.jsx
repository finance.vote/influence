import AvatarImage from 'components/AvatarImage/AvatarImage';
import { useGlobalState } from 'globalStateStore';
import { getExplorerLinkLabel } from 'helpers/profile';
import { explorer } from 'helpers/utils';
import { memo, useEffect, useState } from 'react';
import styles from './Contributors.module.scss';

const Contributors = ({ contributorsData, idHolder, tokenHolder }) => {
  const [address] = useGlobalState('address');
  const isOnlyTokenHolder = tokenHolder && !idHolder;

  const sortContributorsUserFirst = (contributorsData) => {
    if (address && contributorsData.includes(address.toLowerCase())) {
      const { [address.toLowerCase()]: firstKeyValue, ...rest } = contributorsData;
      return {
        [address.toLowerCase()]: firstKeyValue,
        ...rest
      };
    }
    return contributorsData;
  };

  const generateVisibleContributors = (contributorsData, showAllContributors) => {
    return showAllContributors
      ? sortContributorsUserFirst(contributorsData)
      : sortContributorsUserFirst(contributorsData).slice(0, 10);
  };

  const [showAllContributors, setShowAllVotes] = useState(false);
  const [visibleContributors, setVisibleVotes] = useState(
    generateVisibleContributors(contributorsData, false)
  );

  useEffect(() => {
    if (Object.entries(contributorsData).length > 0)
      setVisibleVotes(generateVisibleContributors(contributorsData, showAllContributors));
  }, [showAllContributors]);

  return (
    <div className={styles.container}>
      <table className={styles.grid}>
        <thead>
          <tr className={styles.header}>
            <th className={styles.col}>
              <span>ID NO.</span>
            </th>
            <th className={styles.col}>SUBMISSIONS</th>
          </tr>
        </thead>
        <tbody>
          {contributorsData.length === 0 && (
            <tr>
              <td colSpan="2">
                <p>There aren&apos;t any contributors here yet!</p>
              </td>
            </tr>
          )}
          {contributorsData.length > 0 &&
            visibleContributors.map((contr, index) => {
              return (
                <tr key={`contributorsData-grid-item${index}`} className={styles.item}>
                  <td className={styles.col}>
                    <div className={styles.profile}>
                      <AvatarImage
                        nftAddress={contr.contractAddress}
                        style={{
                          maxWidth: '50px',
                          maxHeight: '50px',
                          marginRight: '18px',
                          marginLeft: '16px'
                        }}
                        voterAddress={contr.walletAddress}
                        voter={contr.userId}
                        isOs1155={contr.isOs1155}
                        voterNetwork={contr.networkId}
                      />
                      <div style={{ alignSelf: 'center' }}>
                        <a
                          href={explorer(contr.networkId || '1', contr.walletAddress)}
                          target="_blank"
                          rel="noopener noreferrer">
                          <button className={styles.profileBtn}>
                            <span>
                              {getExplorerLinkLabel(
                                isOnlyTokenHolder,
                                contr.isOs1155,
                                contr.userId,
                                contr.walletAddress
                              )}
                            </span>
                          </button>
                        </a>
                      </div>
                    </div>
                  </td>
                  <td className={styles.col}>{contr.submissionsCounter}</td>
                </tr>
              );
            })}
        </tbody>
        {contributorsData.length > 10 && !showAllContributors && (
          <tfoot className={styles.footer}>
            <tr>
              <td colSpan="2">
                <button className={styles.seeMoreBtn} onClick={() => setShowAllVotes(true)}>
                  See more
                </button>
              </td>
            </tr>
          </tfoot>
        )}
      </table>
    </div>
  );
};

export default memo(Contributors);
