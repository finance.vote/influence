import classnames from 'classnames';
import influence_icon from 'images/spaces/influence/influence_icon.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './MobileLogo.module.scss';

const MobileLogo = ({ name, src, isOpened }) => {
  return (
    <>
      <Link
        href={{
          pathname: `/`
        }}>
        <span className={classnames(styles.homeLogo, isOpened ? styles.opened : '')}>
          <picture>
            <source
              className={styles.icon}
              media="(max-width: 1000px)"
              srcSet={influence_icon}
              alt="influence-logo"
            />
            <Image className={styles.icon} src={influence_icon} alt="influence-logo" />
          </picture>
        </span>
      </Link>
      {name ? (
        <picture className={classnames(styles.spaceLogo, isOpened ? styles.opened : '')}>
          <source media="(max-width: 1000px)" srcSet={src} alt={`${name}_logo`} />
          <img src={src} alt={`${name}_logo`} />
        </picture>
      ) : null}
    </>
  );
};

export default MobileLogo;
