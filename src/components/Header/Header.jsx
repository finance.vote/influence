import classNames from 'classnames';
import SignInButton from 'components/Siwe/Siwe';
import Loading from 'components/Ui/Loading';
import { ChainDropdown, CustomSignIn } from 'evm-chain-scripts';
import { setGlobalState, useGlobalState } from 'globalStateStore';
import influence_icon from 'images/spaces/influence/influence_icon.svg';
import logo_light from 'images/spaces/influence/logo_light.svg';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import HamburgerIcon from './HamburgerIcon';
import styles from './Header.module.scss';

const customDropdownStyles = {
  option: (defaultStyles, { isSelected }) => {
    return {
      ...defaultStyles,
      backgroundColor: isSelected ? '#45b8ff' : '#0e273b',
      color: 'white',
      cursor: 'pointer',
      paddingTop: '8px',
      paddingRight: '35px',
      paddingBottom: '8px',
      paddingLeft: '10px',
      ':hover': {
        backgroundColor: '#45b8ff'
      }
    };
  },
  singleValue: (defaultStyles) => ({ ...defaultStyles, color: '#adb6bc' }),
  control: (defaultStyles) => ({
    ...defaultStyles,
    border: 'solid 1px #45b8ff',
    backgroundColor: 'transparent',
    fontFamily: 'WorkSans',
    height: '46px',
    borderRadius: 0,
    ':hover': {
      border: 'solid 1px white'
    }
  }),
  menuList: (defaultStyles) => ({
    ...defaultStyles,
    border: 'solid 1px #45b8ff',
    padding: 0
  }),
  dropdownIndicator: (defaultStyles) => ({
    ...defaultStyles,
    svg: {
      fill: '#adb6bc',
      ':hover': {
        fill: 'white'
      }
    }
  }),
  indicatorSeparator: () => ({
    display: 'none'
  })
};

const Account = () => {
  const router = useRouter();
  const [space] = useGlobalState('space');
  const [currentNetwork, setCurrentNetwork] = useGlobalState('currentNetwork');
  const [isMember] = useGlobalState('isMember');
  const [address] = useGlobalState('address');
  const [showChainsDropdown] = useGlobalState('showChainsDropdown');
  const [supportedChains] = useGlobalState('supportedChains');
  const [isMobileMenuOpened, setIsMobileMenuOpened] = useState(false);

  const isConnected = address.length > 0;
  const bigLogoPages = ['/', '/badges', '/admin/add-new-realm', '/:key/admin/edit'];

  const isBigLogoPage = bigLogoPages.includes(router.pathname);

  const spaceLoaded = space && Object.entries(space).length > 0;

  const toggleMobileMenu = () => setIsMobileMenuOpened(!isMobileMenuOpened);

  useEffect(() => {
    if (spaceLoaded && !space.isNamespace) {
      const chains = supportedChains.length ? supportedChains : space?.networks;
      if (!showChainsDropdown) setCurrentNetwork(parseInt(chains[0]));
    }
  }, [space, currentNetwork]);

  const location = typeof window !== 'undefined' && window.location;

  useEffect(() => {
    setIsMobileMenuOpened(false);
  }, [location]);

  const goToSpacePage = () => {
    if (router.asPath.includes('-')) {
      const spaces = router.asPath.split('-');
      router.push(`${spaces[0]}`);
    } else router.push('/');
  };

  const realmLogo =
    space?.logoHash && !space.isNamespace
      ? `https://factorydao.infura-ipfs.io/ipfs/${space.logoHash}`
      : null;

  const namespaceLogo =
    space?.logoHash && space.isNamespace
      ? `https://factorydao.infura-ipfs.io/ipfs/${space.imageHash}`
      : null;

  const chainDropdownCondition = showChainsDropdown && spaceLoaded && !space.isNamespace;
  const CustomSignInCondition = isMember && isConnected;

  return (
    <div
      id="sticky"
      className={classNames(
        styles.mobileMenuWrapper,
        isMobileMenuOpened ? styles.mobileWrapperOpened : ''
      )}
      style={{ zIndex: 20 }}>
      <nav className={styles.topnav}>
        {isBigLogoPage ? (
          <div className={classNames(styles.leftNav)}>
            <Link href="/">
              <span className={styles.image}>
                <Image height={34} className={styles.icon} src={logo_light} alt="influence-logo" />
              </span>
            </Link>
          </div>
        ) : null}
        <div className={classNames(styles.topnavleft, isBigLogoPage ? styles.hidden : null)}>
          <div className={styles.influenceLogo}>
            <Link
              href={{
                pathname: `/`
              }}>
              <Image
                className={styles.icon}
                width={30}
                height={30}
                src={influence_icon}
                alt="influence-logo"
              />
            </Link>
          </div>
        </div>
        <div className={styles.spaceLogoContainer}>
          {spaceLoaded ? (
            realmLogo ? (
              <Image
                onClick={goToSpacePage}
                className={styles.realmLogo}
                src={realmLogo}
                alt={`${space.name}_logo`}
                fill
              />
            ) : (
              <Image
                onClick={goToSpacePage}
                className={styles.realmLogo}
                src={namespaceLogo}
                alt={`${space.name}_logo`}
                fill
              />
            )
          ) : null}
        </div>
        <div
          className={classNames(
            styles.container,
            chainDropdownCondition || CustomSignInCondition
              ? isMobileMenuOpened
                ? styles.isMobileOpened
                : styles.hiddenOnMobile
              : null
          )}>
          <div className={styles.rightnav}>
            <React.Suspense fallback={<Loading />}>
              <SignInButton />
            </React.Suspense>
            {chainDropdownCondition && (
              <ChainDropdown
                className={styles.dropdown}
                optionClassName={styles.option}
                optionIconClassName={styles.optionIcon}
                optionLabelClassName={styles.optionLabel}
                supportedChains={supportedChains ?? space?.networks}
                onChange={(chainId) => setCurrentNetwork(parseInt(chainId))}
                styles={customDropdownStyles}
              />
            )}
            {CustomSignInCondition && (
              <CustomSignIn
                onClose={(address) => setGlobalState('address', address)}
                className={styles.customSignInButton}
              />
            )}
          </div>
        </div>
      </nav>
      {chainDropdownCondition || CustomSignInCondition ? (
        <HamburgerIcon onClick={toggleMobileMenu} isActive={isMobileMenuOpened} styles={styles} />
      ) : null}
    </div>
  );
};

export default Account;
