import classNames from 'classnames';
import AvatarImage from 'components/AvatarImage/AvatarImage';
import If from 'components/If';
import networks from 'configs/networks.json';
import { useGlobalState } from 'globalStateStore';
import { shortenWalletAddress } from 'helpers/influence';
import { explorer, shorten } from 'helpers/utils';
import { memo, useEffect, useState } from 'react';
import Dropdown from 'react-dropdown';
import styles from './Votes.module.scss';

const getQvPowerSum = (choices) => {
  return choices.reduce((sum, choice) => sum + Math.pow(choice.value, 2), 0);
};

const getBinaryPower = (choices) => {
  return choices[0].value;
};

const formatParams = (paramKey) => Object.keys(paramKey ?? {}).filter((address) => address !== '');

const getOptionsForDropdown = (strategy) => {
  const networksArray = Object.values(networks);
  const tokenNetworks = formatParams(strategy.params?.address);
  const nftNetworks = formatParams(strategy.params?.nftAddresses);
  const chains = tokenNetworks
    .concat(nftNetworks.filter((nftChain) => tokenNetworks.indexOf(nftChain) < 0))
    .sort();
  const options = [];
  for (const chainId of chains) {
    const { shortName, name } = networksArray.find((x) => x.key === chainId) || {};
    options.push({
      value: chainId,
      label: shortName ?? name,
      className: classNames(styles.open, styles.option)
    });
  }

  return options;
};

const selectDefaultOption = (votes, dropdownOptions) => {
  let selected = {};
  for (const voter in votes) {
    selected = { ...selected, [voter]: dropdownOptions[0] };
  }
  return selected;
};
/**
 *
 * Influence Votes Component.
 * Shows information about voters and spend tokens
 * Currently used on Proposal Page
 *
 * @param {string} address - wallet address
 * @param {Object} votes - votes data
 * @returns information about voters
 */
const VotesBlock = ({ votes, strategy }) => {
  const [address] = useGlobalState('address');
  const isOs1155 = !!(strategy?.key === 'os-1155');
  const nftAddresses = strategy.params?.nftAddresses;
  const isBinary = strategy.binary;
  const isCombinedChainsVoting = strategy.key === 'combined-chains';
  const getPower = isBinary ? getBinaryPower : getQvPowerSum;
  const explorerDropHandle = (item, voter) => {
    setSelectedVoterChain({ ...selectedVoterChain, [voter]: item });
  };

  const dropdownOptions = getOptionsForDropdown(strategy);
  const [selectedVoterChain, setSelectedVoterChain] = useState(
    selectDefaultOption(votes, dropdownOptions)
  );

  const sortVotesUserFirst = (votes) => {
    if (address && Object.keys(votes).includes(address.toLowerCase())) {
      const { [[address.toLowerCase()]]: firstKeyValue, ...rest } = votes;
      return {
        [[address.toLowerCase()]]: firstKeyValue,
        ...rest
      };
    }
    return votes;
  };

  const generateVisibleVotes = (votes, showAllVotes) => {
    return showAllVotes
      ? sortVotesUserFirst(votes)
      : Object.fromEntries(Object.entries(sortVotesUserFirst(votes)).slice(0, 10));
  };

  const [showAllVotes, setShowAllVotes] = useState(false);
  const [visibleVotes, setVisibleVotes] = useState(generateVisibleVotes(votes, false));

  useEffect(() => {
    if (Object.entries(votes).length > 0)
      setVisibleVotes(generateVisibleVotes(votes, showAllVotes));
  }, [showAllVotes]);

  const generateProfileName = (vote) => {
    // if (address && vote.address.toLowerCase() === address.toLowerCase()) {
    //   return "You";
    // }
    if (vote.profile?.name) {
      return vote.profile.name;
    } else if (vote.profile?.ens) {
      return vote.profile.ens;
    }
    return shorten(vote.address);
  };

  const getExplorerLinkLabel = (isOs1155, voterIds, voterId, vote) => {
    if (isOs1155) {
      return shortenWalletAddress(voterId);
    }
    if (voterIds && voterIds.length > 0) {
      return voterIds;
    }
    return generateProfileName(vote);
  };

  return (
    <div className={styles.container}>
      <table className={styles.grid}>
        <thead>
          <tr className={styles.header}>
            <th className={styles.col}>
              <span>ID NO.</span>
            </th>
            <th className={styles.col}>$I POWER</th>
          </tr>
        </thead>
        <tbody>
          {Object.entries(votes).length === 0 && (
            <tr>
              <td colSpan="2">
                <p>There aren&apos;t any voters here yet!</p>
              </td>
            </tr>
          )}
          {Object.entries(votes).length > 0 &&
            Object.entries(visibleVotes).map((data, index) => {
              const vote = data[1];
              const voterId = Array.isArray(vote.msg?.payload?.voterId)
                ? vote.msg?.payload?.voterId?.[0]
                : vote.msg?.payload?.voterId;

              const voterIds = Array.isArray(vote.msg?.payload?.voterId)
                ? vote.msg?.payload?.voterId.join(', ')
                : vote.msg?.payload?.voterId;

              let voterNetwork = '';
              if (isCombinedChainsVoting) {
                voterNetwork = selectedVoterChain[vote.address].value;
              } else {
                voterNetwork = vote.msg?.payload?.network || '1';
              }
              return (
                <tr key={`votes-grid-item${index}`} className={styles.item}>
                  <td className={styles.col}>
                    <div className={styles.profile}>
                      <AvatarImage
                        nftAddress={nftAddresses ? nftAddresses[voterNetwork] : ''}
                        style={{
                          maxWidth: '50px',
                          maxHeight: '50px'
                        }}
                        voterAddress={vote.address}
                        voter={voterId}
                        isOs1155={isOs1155}
                        voterNetwork={isCombinedChainsVoting ? null : voterNetwork}
                      />
                      <If condition={isCombinedChainsVoting && dropdownOptions.length > 1}>
                        <Dropdown
                          className={styles.dropdown}
                          controlClassName={styles.control}
                          placeholderClassName={styles.placeholder}
                          menuClassName={styles.menu}
                          options={dropdownOptions}
                          placeholder="Explorer"
                          value={selectedVoterChain[vote.address]}
                          onChange={(item) => explorerDropHandle(item, vote.address)}
                          arrowClosed={<span className={styles.arrowClosed} />}
                          arrowOpen={<span className={styles.arrowOpen} />}
                        />
                      </If>
                      <div style={{ alignSelf: 'center' }}>
                        <a
                          href={explorer(voterNetwork, vote.address)}
                          target="_blank"
                          rel="noopener noreferrer">
                          <button className={styles.profileBtn}>
                            <span>{getExplorerLinkLabel(isOs1155, voterIds, voterId, vote)}</span>
                          </button>
                        </a>
                      </div>
                    </div>
                  </td>
                  <td className={styles.col}>{`${getPower(vote.msg.payload.choice)} $I`}</td>
                </tr>
              );
            })}
        </tbody>
        {Object.keys(votes).length > 10 && !showAllVotes && (
          <tfoot className={styles.footer}>
            <tr>
              <td colSpan="2">
                <button className={styles.seeMoreBtn} onClick={() => setShowAllVotes(true)}>
                  See more
                </button>
              </td>
            </tr>
          </tfoot>
        )}
      </table>
    </div>
  );
};

export default memo(VotesBlock);
