import Button from 'components/Ui/Button';
import Icon from 'components/Ui/Icon';
import Label from 'components/Ui/Label';
import uniqueId from 'lodash/uniqueId';
import styles from './ValuesPerChain.module.scss';

const ValuePerChain = ({
  paramName,
  paramKey,
  dynamicParam,
  valueHeaderText = 'Contract Address',
  setFieldValue,
  errors,
  values
}) => {
  const chainItems = values[paramKey].dynamicParamData;
  const { defaultValue, dynamicParamData, originParamData } = dynamicParam;

  const handleCheckboxChange = (checked) => {
    setFieldValue(paramKey, {
      defaultValue: checked,
      dynamicParamData: checked ? originParamData : dynamicParamData,
      originParamData
    });
  };

  const getChainItem = (itemId) => {
    const chain = chainItems.find((chain) => chain.itemId === itemId);
    const index = chainItems.indexOf(chain);
    return { chain, index };
  };

  const editChainItem = (editedChain, index) => {
    const newItems = [...chainItems];
    newItems.splice(index, 1);
    newItems.splice(index, 0, editedChain);
    saveParam(newItems);
    return newItems;
  };

  const saveParam = (items) => {
    setFieldValue(paramKey, { defaultValue: false, dynamicParamData: items, originParamData });
  };

  const handleAddChain = () => {
    const items = chainItems.concat({
      itemId: parseInt(uniqueId()),
      chainId: '',
      value: '',
      errors: {}
    });

    saveParam(items);
  };

  const handleUpdateChain = (itemId, event) => {
    const { chain, index } = getChainItem(itemId);
    const name = event.target.name;
    const value = event.target.value;

    let editedChainItems = [];
    if (chain) {
      let editedChain = {
        ...chain,
        [name]: value
      };

      editedChainItems = editChainItem(editedChain, index);
    }

    saveParam(editedChainItems);
  };

  const handleDeleteChain = (itemId) => {
    const { index } = getChainItem(itemId);

    if (index >= 0) {
      const newItems = [...chainItems];
      newItems.splice(index, 1);

      saveParam(newItems);
    }
  };

  const inputError = errors[paramKey]?.dynamicParamData ? errors[paramKey]?.dynamicParamData : [];

  return (
    <>
      <div className={styles.container}>
        <div className={styles.left}>
          <div>{paramName}:</div>
          <div className={styles.editDefault}>
            <input
              name="defaultData"
              className={styles.info}
              defaultChecked={defaultValue}
              type="checkbox"
              onChange={(event) => handleCheckboxChange(event.target.checked)}
            />
            <Label text="Default" />
          </div>
        </div>
        <div className={styles.right}>
          <div className={styles.list}>
            <div className={styles.chainRow}>
              <div className={styles.item}>Chain ID</div>
              <div className={styles.item}>{valueHeaderText}</div>
              <div className={styles.item}>Action</div>
            </div>

            {chainItems.map(({ itemId, value, chainId }, index) => {
              return (
                <div key={index}>
                  <div className={styles.chainRow}>
                    <div className={styles.item}>
                      <input
                        placeholder="ChainId"
                        value={chainId}
                        name="chainId"
                        type="text"
                        className="input"
                        disabled={defaultValue}
                        onChange={(event) => handleUpdateChain(itemId, event)}
                      />
                      {inputError[index]?.chainId && (
                        <p style={{ color: 'red', fontSize: 16 }}>{inputError[index]?.chainId}</p>
                      )}
                    </div>
                    <div className={styles.item}>
                      <input
                        placeholder={valueHeaderText}
                        value={value}
                        type="text"
                        name="value"
                        className="input"
                        disabled={defaultValue}
                        onChange={(event) => handleUpdateChain(itemId, event)}
                      />
                      {inputError[index]?.value && (
                        <p style={{ color: 'red', fontSize: 16 }}>{inputError[index]?.value}</p>
                      )}
                    </div>
                    <div className={styles.item}>
                      <Icon
                        className={defaultValue ? styles.disabledIcon : styles.icon}
                        name="close"
                        size="22"
                        onClick={defaultValue ? null : () => handleDeleteChain(itemId)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}

            <Button className={styles.button} disabled={defaultValue} onClick={handleAddChain}>
              Add Next Chain
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ValuePerChain;
