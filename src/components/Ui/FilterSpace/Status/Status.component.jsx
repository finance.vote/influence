import styles from './Status.module.scss';

const Status = ({ onChangeHandler, statuses, options }) => {
  return (
    <div className={styles.statusContainer}>
      {options.map((option, index) => {
        const isChecked = statuses.find((status) => status === option);
        return (
          <label key={index} className={styles.statusLabel}>
            <input
              type="checkbox"
              key={index}
              id={`status-checkbox-${index}`}
              name={option}
              value={option}
              checked={isChecked}
              onChange={(event) => onChangeHandler(event, option, 'status')}
            />
            {option === 'awaiting' ? 'pending' : option}
          </label>
        );
      })}
    </div>
  );
};

export default Status;
