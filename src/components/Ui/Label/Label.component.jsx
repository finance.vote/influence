import classnames from 'classnames';
import classes from './Label.module.scss';

const Label = ({ text }) => {
  return <span className={classnames(classes.Label, `border v-align-middle ml-1`)}>{text}</span>;
};

export default Label;
