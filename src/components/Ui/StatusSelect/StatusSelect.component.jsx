import classNames from 'classnames';
import { STATUSES } from 'helpers/statuses';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import styles from './StatusSelect.module.scss';

const getPlaceHolderClassName = ({ value }) => {
  switch (value) {
    case STATUSES.CLOSED:
      return styles.closedColor;
    case STATUSES.OPEN:
      return styles.openColor;
    case STATUSES.AWAITING:
      return styles.awaitingColor;
    case STATUSES.DRAFT:
      return styles.draftColor;
    case STATUSES.RANKING:
      return styles.rankingColor;
    default:
      return styles.placeholder;
  }
};

const StatusSelect = ({ status, onStatusChange, onStatusReset, tasks = false }) => {
  let dropDownOptionsStatus = [
    {
      value: STATUSES.OPEN,
      label: (
        <>
          <div>Open</div>
          <div></div>
        </>
      ),
      className: classNames(styles.open, styles.option)
    },
    {
      value: STATUSES.DRAFT,
      label: (
        <>
          <div>Draft</div>
          <div></div>
        </>
      ),
      className: classNames(styles.draft, styles.option)
    },
    {
      value: STATUSES.AWAITING,
      label: (
        <>
          <div>Awaiting</div>
          <div></div>
        </>
      ),
      className: classNames(styles.awaiting, styles.option)
    },
    // TODO: add styles and uncomment code whenever prevote feature will be ready
    // {
    //   value: STATUSES.PREVOTE,
    //   label: (
    //     <>
    //       <div>Pre-vote</div>
    //       <div></div>
    //     </>
    //   ),
    //   className: classNames(styles.prevote, styles.option)
    // },
    {
      value: STATUSES.CLOSED,
      label: (
        <>
          <div>Closed</div>
          <div></div>
        </>
      ),
      className: classNames(styles.closed, styles.option)
    },
    {
      value: STATUSES.RANKING,
      label: (
        <>
          <div>Ranking</div>
          <div className={styles.icon}>
            <div className={classNames(styles.arrow, styles.arrowUp)}></div>
            <div className={classNames(styles.arrow, styles.arrowDown)}></div>
          </div>
        </>
      ),
      className: classNames(styles.ranking, styles.option)
    }
  ];

  if (tasks) {
    dropDownOptionsStatus = dropDownOptionsStatus.filter(
      (option) => option.value !== STATUSES.DRAFT
    );
  } else {
    dropDownOptionsStatus = dropDownOptionsStatus.filter(
      (option) => option.value !== STATUSES.RANKING
    );
  }

  return (
    <Dropdown
      className={styles.statusSelect}
      controlClassName={styles.control}
      placeholderClassName={getPlaceHolderClassName(status)}
      menuClassName={styles.menu}
      options={dropDownOptionsStatus}
      placeholder="Status"
      value={status}
      onChange={onStatusChange}
      arrowClosed={
        <span
          aria-hidden="true"
          onClick={onStatusReset}
          className={
            status.value !== 'all' ? styles.cross : classNames(styles.arrow, styles.arrowClosed)
          }
        />
      }
      arrowOpen={
        <span
          aria-hidden="true"
          onClick={onStatusReset}
          className={
            status.value !== 'all' ? styles.cross : classNames(styles.arrow, styles.arrowOpen)
          }
        />
      }
    />
  );
};

export default StatusSelect;
