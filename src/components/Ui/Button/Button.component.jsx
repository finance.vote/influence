import classNames from 'classnames';
import Loading from '../Loading';
import styles from './Button.module.scss';
/**
 *
 * Button component
 *
 * @param {string} type - button type
 * @param {Function} onClick - on click action
 * @param {Boolean} disabled - button disabled
 * @param {Boolean} loading - show/hide loading - confirming the action execution
 * @returns Button component
 */
const Button = ({ type, onClick, disabled, className, loading, children, dataTest = '' }) => {
  return (
    <button
      type={type || 'button'}
      onClick={onClick}
      className={classNames(styles.button, className)}
      disabled={disabled || false}
      data-test={dataTest}>
      {loading && <Loading />}
      {!loading && children}
    </button>
  );
};

export default Button;
