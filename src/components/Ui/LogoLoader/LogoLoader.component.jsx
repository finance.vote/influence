import loader from 'images/spaces/influence/loader.svg';
import Image from 'next/image';

const LogoLoader = ({ classes }) => {
  return <Image alt="influence-loader" src={loader} className={classes} />;
};

export default LogoLoader;
