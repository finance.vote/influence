import { setGlobalState } from 'globalStateStore';
import Icon from '../Icon';
import styles from './TableHeader.module.scss';

const TableHeader = ({ gridHeaders, sortingData, tableSortingName, tableActivePageName }) => {
  const onSortChange = (columnId) => {
    setGlobalState(tableActivePageName, 1);

    if (columnId === sortingData.column) {
      switch (sortingData.direction) {
        case null:
          setGlobalState(tableSortingName, { ...sortingData, direction: 1 });
          break;
        case 1:
          setGlobalState(tableSortingName, { ...sortingData, direction: -1 });
          break;
        case -1:
          setGlobalState(tableSortingName, { column: null, direction: null });
          break;
      }
    } else {
      setGlobalState(tableSortingName, { column: columnId, direction: 1 });
    }
  };

  return (
    <>
      {gridHeaders?.map((header, index) => {
        const isActive = sortingData.column === header.id;
        return (
          <th key={index} scope="col" className={styles.headerCol}>
            {header.isSortable ? (
              <span
                role="button"
                tabIndex={0}
                className={styles.headerSort}
                onClick={() => onSortChange(header.id)}
                onKeyDown={() => onSortChange(header.id)}>
                <span>{header.name}</span>
                <span className={styles.arrows}>
                  <div className={styles.sortTableContainer}>
                    <Icon
                      name="arrow-up"
                      className={
                        isActive && sortingData.direction === 1 ? styles.sortTableActive : ''
                      }
                    />
                    <Icon
                      name="arrow-down"
                      className={
                        isActive && sortingData.direction === -1 ? styles.sortTableActive : ''
                      }
                    />
                  </div>
                </span>
              </span>
            ) : (
              header.name
            )}
          </th>
        );
      })}
    </>
  );
};

export default TableHeader;
