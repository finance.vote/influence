import Image from 'next/image';
import IdentityFvtImage from './Identity-id.png';
import classes from './IdentityFvtId.module.scss';

/**
 *
 * Identity image component
 * Used in opinions list in identity space
 *
 * @param {object} - styles
 * @param {value} - value, id of FVT
 * @returns Identity image component
 */
const IdentityFvtId = ({ style, value }) => {
  return (
    <div className={classes.container} style={{ ...style }}>
      <div className={classes.inner}>
        <Image src={IdentityFvtImage} className={classes.image} alt="Identity Voting" />
        <div className={classes.textCotainer}>
          <div className={classes.textLabel}>FVT</div>
          <div className={classes.text}>{value}</div>
        </div>
      </div>
    </div>
  );
};

export default IdentityFvtId;
