import classnames from 'classnames';
import Icon from 'components/Ui/Icon';
import { Field } from 'formik';

import styles from './SimpleInput.module.scss';

export const SimpleInput = ({ proposalCreators, elementsList, setFieldValue, errors }) => {
  const addNewElement = () => {
    const newElement = proposalCreators?.length
      ? proposalCreators.reduce((acc, curr) => {
          acc.index = (curr.index ?? 0) + 1;
          acc.address = '';
          return acc;
        }, {})
      : { index: 0, address: '' };
    const updatedProposalCreators = [...proposalCreators, newElement];
    return setFieldValue(elementsList, updatedProposalCreators);
  };

  const deleteItem = (index) => {
    if (proposalCreators.length === 1) return;
    const indexToDelete = proposalCreators.findIndex((item) => item.index === index);
    proposalCreators.splice(indexToDelete, 1);
    setFieldValue(elementsList, [...proposalCreators]);
  };

  const updateElements = (event, index) => {
    const {
      target: { value }
    } = event;
    if (proposalCreators[index]) proposalCreators[index].address = value;
    setFieldValue(elementsList, proposalCreators);
  };
  return (
    <>
      <div data-test="simple-input-proposal-creators-elements">
        {proposalCreators.map(({ address, index }) => {
          const showError = errors?.[index];
          return (
            <div key={index}>
              <div className={styles.argumentInputs}>
                <Field
                  value={address}
                  type="text"
                  placeholder="Wallet address"
                  onChange={(e) => updateElements(e, index)}
                  data-test="simple-input-element"
                />
                <Icon
                  className={proposalCreators.length > 1 ? styles.icon : styles.disabledIcon}
                  name="close"
                  size="22"
                  onClick={() => deleteItem(index)}
                  data-test="delete-simple-element"
                />
              </div>
              {showError ? (
                <span className={styles.multiSelectError}>{errors[index]?.address}</span>
              ) : null}
            </div>
          );
        })}
      </div>
      <button
        className={classnames(styles.hollowButton, styles.hollowButtonBlack, styles.addElementBtn)}
        onClick={addNewElement}
        type="button"
        data-test="simple-input-add-new-element">
        Add new wallet address
      </button>
    </>
  );
};
