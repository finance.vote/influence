import classNames from 'classnames';
import If from 'components/If';
import LogoLoader from 'components/Ui/LogoLoader';
import Identity from 'contracts/Identity.json';
import { useGlobalState } from 'globalStateStore';
import { useFetchNftImage } from 'helpers/queries';
import { useEffect, useState } from 'react';
import styles from './NftImage.module.scss';

const nftGalleryContracts = [
  ...Object.values(Identity.networks).map((item) => item.address.toLowerCase()),
  '0xd5e00f2a37a7741332f64bc407ebab764c4c69c1'
];

const NftImage = ({
  style,
  nftId,
  contractAddress,
  center = false,
  isOs1155 = false,
  chainId = null
}) => {
  const [nftImage, setNftImage] = useState('');
  const [isVideo, setIsVideo] = useState(false);
  const [currentChainId] = useGlobalState('currentNetwork');
  const [isLoading, setIsLoading] = useState(true);
  const isSingleChainIdentity = Array.isArray(contractAddress)
    ? false
    : nftGalleryContracts.includes(contractAddress.toLowerCase());

  const { data: nftMedia, isFetching: isNftMediaFetching } = useFetchNftImage(
    nftId,
    contractAddress,
    chainId || currentChainId,
    isOs1155,
    isSingleChainIdentity
  );
  useEffect(() => {
    if (nftMedia) {
      setIsVideo(nftMedia.isVideo);
      setNftImage(nftMedia.mediaUrl);
    }
  }, [nftMedia]);

  return (
    <>
      <If condition={nftId}>
        <If condition={isVideo}>
          <video
            autoPlay
            muted
            loop
            playsInline
            className={styles.image}
            style={{
              ...style,
              display: isNftMediaFetching || isLoading ? 'none' : 'block'
            }}
            onLoadStart={() => setIsLoading(false)}>
            <source src={nftImage} type="video/mp4" />
          </video>
        </If>
        <If condition={!isVideo}>
          {/* eslint-disable-next-line @next/next/no-img-element */}
          <img
            alt="nft"
            src={nftImage}
            className={styles.image}
            style={{
              ...style,
              display: isNftMediaFetching || isLoading ? 'none' : 'block'
            }}
            onLoad={() => setIsLoading(false)}
          />
        </If>
      </If>
      <If condition={isLoading || isNftMediaFetching}>
        <LogoLoader classes={classNames(styles.imgspinner, center ? styles.spinnercenter : '')} />
      </If>
    </>
  );
};

export default NftImage;
