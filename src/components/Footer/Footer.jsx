import Container from 'components/Ui/Container';
import mediumIcon from 'images/medium.svg';
import telegramIcon from 'images/telegram.svg';
import twitterIcon from 'images/x-icon-blue.png';
import Image from 'next/image';
import styles from './Footer.module.scss';

const Footer = () => {
  return (
    <footer className={styles.container}>
      <Container>
        <div className={styles.footer}>
          <div>
            {'2021 All rights reserved  '}
            <a href="https://www.factorydao.org/" target="_blank" rel="noreferrer">
              FactoryDAO
            </a>
            <span className={styles.version}>v. {process.env.NEXT_PUBLIC_VERSION}</span>
          </div>
          <div className={styles.links}>
            <a href="https://t.me/factdao" target="_blank" rel="noreferrer">
              <Image src={telegramIcon} alt="telegram" />
            </a>
            <a href="https://twitter.com/FactDAO" target="_blank" rel="noreferrer">
              <Image src={twitterIcon} className={styles.xIcon} alt="Twitter" />
            </a>
            <a href="https://medium.com/@financedotvote" target="_blank" rel="noreferrer">
              <Image src={mediumIcon} alt="Medium" />
            </a>
          </div>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
