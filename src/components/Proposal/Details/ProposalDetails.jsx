import Markdown from 'components/Ui/Markdown';
import Sign from 'components/Ui/Sign';
import State from 'components/Ui/State';
import styles from './ProposalDetails.module.scss';

const ProposalDetails = ({ payload, signData, isDraft }) => {
  const isServer = typeof window === 'undefined';
  return (
    <div className={styles.description}>
      <State startDate={payload.start} endDate={payload.end} isDraft={isDraft} textVisible />
      <div className={styles.title}>{payload?.name}</div>
      <Sign color={signData?.bgColor}>{signData?.text}</Sign>
      {!isServer ? <Markdown body={payload.body} className={styles.markdown} /> : null}
    </div>
  );
};

export default ProposalDetails;
