import classes from './MultipleVotingNotification.module.scss';

export default function MultipleVotingNotification({ ids = null, isLoading }) {
  let notificationBody = '';

  if (isLoading) {
    notificationBody = 'Loading...';
  } else {
    notificationBody = ids
      ? `Voting with IDs ${Object.keys(ids).join(', ')}`
      : 'Multiple ID voting ballot';
  }

  return <div className={classes.container}>{notificationBody}</div>;
}
