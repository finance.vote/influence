import If from 'components/If';
import InfluenceVoteModal from 'components/Modal/Vote/Confirm/Influence/InfluenceVote.component';
import VotesBlock from 'components/Spaces/Influence/Blocks/Votes/Votes.component';
import { useProposalState } from 'globalStateStore';
import { useCalcVotesAndOpinions } from 'helpers/queries';
import dynamic from 'next/dynamic';
import { memo, useEffect } from 'react';
import OpinionsTab from '../Opinions/Opinions';
import CombinedVoteComponent from '../VoteComponents/CombinedVoteComponent';
import styles from './ProposalTabs.module.scss';

const BasicVoteComponent = dynamic(() => import('../VoteComponents/BasicVoteComponent'));
const FvtNftVoteComponent = dynamic(() => import('../VoteComponents/FvtNftVoteComponent'));
const NftVoteComponent = dynamic(() => import('../VoteComponents/NftVoteComponent'));
const OS1155VoteComponent = dynamic(() => import('../VoteComponents/OS1155VoteComponent'));

const buyFVTLink =
  'https://app.sushi.com/swap?outputCurrency=0xF7eF90B602F1332d0c12cd8Da6cd25130c768929&inputCurrency=ETH';

const ProposalTabs = ({ tab, refetchProposalData }) => {
  const [initData] = useProposalState('initData');
  const [choices] = useProposalState('choices');
  const [, setTotalI] = useProposalState('totalI');
  const [, setOpinions] = useProposalState('opinions');
  const [voteDialogOpen] = useProposalState('voteDialogOpen');

  const {
    id,
    votes,
    isProposalVisible,
    proposal: { strategy }
  } = initData;
  const { data: votesAndOpinions } = useCalcVotesAndOpinions(votes, choices, id, strategy?.binary);

  useEffect(() => {
    if (!votesAndOpinions) return;
    const { sum, opinions } = votesAndOpinions;
    setTotalI(sum);
    setOpinions(opinions);
  }, [votesAndOpinions]);

  let VoteComponent;

  switch (strategy?.key) {
    case 'nft':
    case 'nft-binary-voting':
    case 'nft-sum':
    case 'nft-sum-binary':
    case 'specific-nft-qv':
    case 'specific-nft-binary':
    case 'nft-metadata':
      VoteComponent = NftVoteComponent;
      break;
    case 'os-1155':
      VoteComponent = OS1155VoteComponent;
      break;
    case 'fvt-nft':
    case 'fvt-nft-sum-binary':
    case 'fvt-nft-binary-voting':
    case 'fvt-nft-sum':
      VoteComponent = FvtNftVoteComponent;
      break;
    case 'combined-chains':
      VoteComponent = CombinedVoteComponent;
      break;
    default:
      VoteComponent = BasicVoteComponent;
  }

  return (
    <>
      <div className={styles.row}>
        <If condition={tab === 'vote' && !isProposalVisible}>
          <div className={styles.mintIdentityInfo}>
            <If condition={strategy?.key === 'semantic-vote-power'}>
              <p>Buy FVT Tokens to access proposal results</p>
              <a href={buyFVTLink} className={styles.linkbtn}>
                Buy FVT
              </a>
            </If>
            <If condition={strategy?.key !== 'semantic-vote-power'}>
              <p>Mint your identity to access proposal results</p>
              {strategy?.mintingLink && (
                <a href={strategy.mintingLink} className={styles.linkbtn}>
                  Mint identity
                </a>
              )}
            </If>
          </div>
        </If>
        <If condition={['myVote', 'vote'].includes(tab)}>
          <VoteComponent strategy={strategy} tab={tab} />
        </If>
        <If condition={tab === 'vlist'}>
          <VotesBlock votes={votes} strategy={strategy} />
        </If>

        <If condition={tab === 'opinions'}>
          <OpinionsTab strategy={strategy} />
        </If>
      </div>
      <If condition={voteDialogOpen}>
        <InfluenceVoteModal
          strategy={initData?.proposal?.strategy}
          refetchProposalData={refetchProposalData}
        />
      </If>
    </>
  );
};

export default memo(ProposalTabs);
