import classNames from 'classnames';
import If from 'components/If';
import MyInfluence from 'components/Spaces/Influence/Blocks/MyInfluence';
import { useGlobalState, useProposalState } from 'globalStateStore';
import { checkIfIdentityOnLists } from 'helpers/proposal';
import {
  useGetMyInfluenceProps,
  useGetVoteComponentProps,
  useLoadChoicesData,
  useMultiChainScoreQuery,
  useScoreQuery,
  useUserRank
} from 'helpers/queries';
import dynamic from 'next/dynamic';
import { memo, useEffect, useState } from 'react';
import styles from './VoteComponent.module.scss';

const Binary = dynamic(() => import('components/Spaces/Influence/Blocks/Binary'));
const QuadraticVote = dynamic(() => import('components/Spaces/Influence/Blocks/QuadraticVote'));

const BasicVoteComponent = ({ strategy, tab }) => {
  const [address] = useGlobalState('address');
  const [currentNetwork] = useGlobalState('currentNetwork');

  const [choices, setChoices] = useProposalState('choices');
  const [initData] = useProposalState('initData');
  const [myChoices, setMyChoices] = useProposalState('myChoices');
  const [unsortedChoices, setUnsortedChoices] = useProposalState('unsortedChoices');
  const [loading] = useProposalState('loading');

  const [isVotePermission, setIsVotePermission] = useState(false);
  const [totalI] = useProposalState('totalI');
  const [tokensLeft, setTokensLeft] = useState('0');
  const [initBalance, setInitBalance] = useState(0);
  const [infResults, setInfResults] = useState([]);
  const [positions, setPositions] = useState(null);

  const {
    id,
    isOpenVote,
    hasMyVotesVoted,
    payload,
    votes,
    isProposalVisible,
    results,
    isDraft,
    proposal,
    hackathonView
  } = initData;

  const { data: score, isLoading: tokensLoading } =
    initData.proposal?.strategy?.params.type === 'sim-multi'
      ? // eslint-disable-next-line react-hooks/rules-of-hooks
        useMultiChainScoreQuery({ address, proposalHash: id })
      : // eslint-disable-next-line react-hooks/rules-of-hooks
        useScoreQuery({
          address,
          currentNetwork,
          proposalHash: id,
          identity: '',
          strategy
        });

  const { data: rank } = useUserRank({
    address,
    currentNetwork,
    proposalHash: id,
    strategyKey: strategy.key
  });

  useEffect(() => {
    if (proposal && rank) {
      const label = strategy.key === 'deposit-rank' ? 'Level' : 'Rank';
      setPositions({ ...rank, label });
    }
  }, [rank]);

  useEffect(() => {
    const _score = score ? score.split('.')[0] : '0';
    setTokensLeft(_score ?? 0);
    setInitBalance(_score ?? 0);
    setInfResults(tab === 'vote' ? results?.influenceResult : results?.myInfluenceResult);
  }, [tab, score, results]);

  const { data: choicesData, isFetching: choicesDataFetching } = useLoadChoicesData(
    infResults,
    payload,
    tab
  );

  useEffect(() => {
    if (!choicesData || loading) return;
    const [unsortedChoices, sortedChoices] = choicesData;
    setMyChoices(sortedChoices);
    setUnsortedChoices(unsortedChoices);
    setChoices(sortedChoices);
  }, [choicesData]);

  useEffect(() => {
    const votePermission = !isDraft
      ? checkIfIdentityOnLists(currentNetwork, payload, address)
      : false;
    setIsVotePermission(!!votePermission);
  }, [address, score, payload.length]);

  const { data: myInfluenceProps, isFetching: myInfluencePropsFetching } = useGetMyInfluenceProps(
    tab,
    isProposalVisible,
    address,
    totalI,
    strategy,
    choices
  );

  const { data: voteComponentProps, isFetching: voteComponentPropsFetching } =
    useGetVoteComponentProps(address, strategy, setUnsortedChoices, setTokensLeft, tab, choices);

  const VotingComponent = strategy?.binary ? Binary : QuadraticVote;

  const isMyInfluenceLoading = choicesDataFetching || myInfluencePropsFetching;
  const isVotesLoading = choicesDataFetching || voteComponentPropsFetching;

  return (
    <>
      <If condition={tab === 'myVote'}>
        <div className={styles.voteData}>
          <MyInfluence
            isLoading={isMyInfluenceLoading}
            isOpenVote={isOpenVote}
            hasVoted={hasMyVotesVoted}
            items={unsortedChoices}
            thirdData={positions}
            secondData={`${Math.floor(initBalance)} ${strategy?.params?.symbol || ''}`}
            hackathonView={hackathonView}
            {...myInfluenceProps}
          />
        </div>
        <div className={classNames(styles.mobilepadding, styles.ballot)}>
          <VotingComponent
            isLoading={isVotesLoading}
            isMyVotesTab
            isOpenVote={isOpenVote}
            hasVoted={hasMyVotesVoted}
            choices={myChoices}
            items={unsortedChoices}
            tokensLeft={tokensLeft}
            setChoices={setMyChoices}
            isVotePermission={isVotePermission}
            tokensLoading={tokensLoading}
            tokensAvailable={initBalance}
            hackathonView={hackathonView}
            {...voteComponentProps}
          />
        </div>
      </If>
      <If condition={tab === 'vote' && choices.length && isProposalVisible}>
        <div className={styles.voteData}>
          <MyInfluence
            isLoading={isMyInfluenceLoading}
            isOpenVote={isOpenVote}
            items={choices}
            secondData={isProposalVisible && Object.keys(votes).length}
            hackathonView={hackathonView}
            {...myInfluenceProps}
          />
        </div>
        <div className={classNames(styles.mobilepadding, styles.ballot)}>
          <QuadraticVote
            isLoading={isVotesLoading}
            isOpenVote={isOpenVote}
            choices={choices}
            items={choices}
            tokensLeft={tokensLeft}
            setChoices={setChoices}
            tokensAvailable={initBalance}
            hackathonView={hackathonView}
            {...voteComponentProps}
          />
        </div>
      </If>
    </>
  );
};

export default memo(BasicVoteComponent);
