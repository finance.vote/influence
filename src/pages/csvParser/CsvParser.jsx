import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import { useState } from 'react';
import styles from './CsvParser.module.scss';

const CsvParser = () => {
  const [csvToParse, setCsvToParse] = useState('');
  const [parsedCsv, setParsedCsv] = useState('');

  const parseCSV = () => {
    // add "" after first occurrence of , in every line, e.g. ( title,text,".xx ==> title,"text,".xx" )
    const parsedString = csvToParse.replace(/,(.*)/gm, ',"$1"');
    setParsedCsv(parsedString);
  };

  return (
    <Container>
      <div className={styles.container}>
        <label className={styles.label}>
          <h2>CSV content to parse</h2>
          <textarea
            className={styles.opinion}
            placeholder="Input CSV content"
            onChange={(e) => setCsvToParse(e.target.value)}
            value={csvToParse}
          />
        </label>
        <div className={styles.buttonContainer}>
          <Button className={styles.button} onClick={parseCSV}>
            Parse
          </Button>
        </div>
        <label className={styles.label}>
          <h2>Parsed CSV content</h2>
          <textarea className={styles.opinion} value={parsedCsv} />
        </label>
      </div>
    </Container>
  );
};

export default CsvParser;
