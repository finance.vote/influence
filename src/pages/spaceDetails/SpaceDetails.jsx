import Container from 'components/Ui/Container';
import { useGlobalState } from 'globalStateStore';
import ProposalSection from './section/proposal/ProposalSection';
import SubmissionSection from './section/submission/SubmissionSection';
import ProposalItemComponent from './singleRow/proposal/ProposalRow';
import QuestionRowComponent from './singleRow/submission/QuestionRow';
import styles from './SpaceDetails.module.scss';

const SpaceDetails = () => {
  const [space] = useGlobalState('space');
  const [isMember] = useGlobalState('isMember');
  const [address] = useGlobalState('address');

  const commonProps = {
    space,
    isMember,
    address
  };

  const proposalsProps = {
    ...commonProps,
    sectionKey: 'proposal',
    header: 'votes',
    ItemComponent: ProposalItemComponent
  };

  const submissionsProps = {
    ...commonProps,
    sectionKey: 'task',
    header: 'submissions',
    ItemComponent: QuestionRowComponent
  };

  return (
    <Container>
      <div className={styles.container}>
        <ProposalSection {...proposalsProps} />
        <SubmissionSection {...submissionsProps} />
      </div>
    </Container>
  );
};

export default SpaceDetails;
