import axios from 'axios';
import { useGlobalState } from 'globalStateStore';
import { getSignOptions } from 'helpers/lists';
import { formatProposals } from 'helpers/utils';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import Section from '../Section';

const ProposalSection = (props) => {
  const { space, address, isMember } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [loadingDraftDeletion, setLoadingDraftDeletion] = useState(false);
  const [signOptions, setSignOptions] = useState([]);

  const [proposalListFilters] = useGlobalState('proposalListFilters');
  const [proposalListActivePage] = useGlobalState('proposalListActivePage');
  const [proposalListSorting] = useGlobalState('proposalListSorting');
  const [proposalStats, setProposalStats] = useState({ pagesCount: 1, total: 0 });
  const [proposalsState, setProposalsState] = useState([]);

  const onDeleteButtonClick = async (event, id) => {
    event.stopPropagation();
    try {
      setLoadingDraftDeletion(true);

      const requestParams = {
        id,
        address,
        spaceKey: space.key
      };

      const { status } = await axios.delete(`/api/proposals/${id}`, { data: requestParams });

      if (status === 200) toast.success(`Successfully deleted draft`, { position: 'top-center' });
      await loadData();
    } catch (error) {
      console.error('Deletion draft proposal error: ' + error);
    } finally {
      setLoadingDraftDeletion(false);
    }
  };

  const onPublishButtonClick = async (event, id) => {
    event.stopPropagation();
    try {
      setLoadingDraftDeletion(true);
      const requestParams = {
        id,
        address,
        spaceKey: space.key
      };
      const { status } = await axios.patch(`/api/proposals/${id}`, requestParams);

      if (status === 200) toast.success(`Successfully published draft`, { position: 'top-center' });
      await loadData();
    } catch (error) {
      console.error('Error while publishing draft: ' + error);
    } finally {
      setLoadingDraftDeletion(false);
    }
  };

  /**
   * Gets data from API about all proposals base on space
   *
   * @returns array of proposals
   */
  const getProposals = async (
    spaceKey,
    proposalListActivePage,
    proposalListFilters,
    { column: sortColumnId, direction: sortDirection },
    searchValue = ''
  ) => {
    const requestParams = {
      pageNumber: proposalListActivePage,
      itemsPerPage: proposalListFilters.itemsPerPage,
      signId: proposalListFilters.sign.join(),
      status: proposalListFilters.status.join(),
      sortColumnId,
      sortDirection,
      searchValue
    };
    try {
      const { data } = await axios.get(`/api/spaces/${spaceKey}/proposals`, {
        params: requestParams
      });
      if (data) {
        const proposalsResult = {
          proposals: Object.entries(formatProposals(data.proposals)),
          totalCount: data.totalCount
        };

        return proposalsResult;
      }
      return [];
    } catch (error) {
      console.log('GET_PROPOSALS_FAILURE', error);
      return [];
    }
  };

  const loadData = async () => {
    setIsLoading(true);

    if (!space.key) return;
    try {
      const { proposals = [], totalCount = 0 } = await getProposals(
        space.key,
        proposalListActivePage,
        proposalListFilters,
        proposalListSorting
      );

      const signOptions = await getSignOptions(space.key);

      const pagesCount = Math.ceil(totalCount / Number(proposalListFilters.itemsPerPage));
      setProposalStats({ pagesCount, total: totalCount });

      const proposalsWithSign = proposals.map((proposal) => {
        const signProps = signOptions?.find((x) => x.id === proposal[1].msg.payload.sign) ?? '';
        return {
          ...proposal,
          sign: signProps.text,
          color: signProps.color || '#00b1ff'
        };
      });

      setSignOptions(signOptions);
      setProposalsState(proposalsWithSign);
    } catch (error) {
      console.log('Proposals list loading error:', error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    (async () => await loadData())();
  }, [space.key, proposalListActivePage, proposalListFilters, proposalListSorting]);

  return (
    <Section
      {...props}
      createLink={`/${space.key}/admin/create`}
      isLoading={isLoading || loadingDraftDeletion}
      data={proposalsState}
      stats={proposalStats}
      signOptions={signOptions}
      onDeleteButtonClick={onDeleteButtonClick}
      onPublishButtonClick={onPublishButtonClick}
      isMember={isMember}
      listFilterIndicator="proposal"
    />
  );
};

export default ProposalSection;
