import infoIcon from 'assets/infoIcon.svg';
import axios from 'axios';
import If from 'components/If';
import { SimpleInput } from 'components/SimpleInput/SimpleInput';
import Addresses from 'components/Spaces/NftAddresses';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import Dialog from 'components/Ui/Dialog';
import TextInput from 'components/Ui/Formik/TextInput';
import PageLoading from 'components/Ui/PageLoading';
import Select from 'components/Ui/Select';
import networks from 'configs/networks.json';
import { toChecksumAddress } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import { useGlobalState } from 'globalStateStore';
import { REALM_RESTRICTION_TYPE, defaultNftAddresses } from 'helpers/spaceForm';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import Dropdown from 'react-dropdown';

import { toast } from 'react-toastify';
import ReactTooltip from 'react-tooltip';
import * as Yup from 'yup';
import styles from './SpaceForm.module.scss';

const REALM_RESTRICTION_OPTIONS = [
  {
    value: REALM_RESTRICTION_TYPE.STANDARD,
    label: 'Wallet address based',
    className: styles.option
  },
  {
    value: REALM_RESTRICTION_TYPE.NFT,
    label: 'NFT based',
    className: styles.option
  }
];

const validateContractAddress = (contractAddress) => {
  let isValid = false;
  try {
    toChecksumAddress(contractAddress);
    isValid = true;
  } catch (error) {
    console.log(error);
  }
  return isValid;
};
const isValidAddress = (value) => {
  try {
    toChecksumAddress(value);
  } catch {
    return false;
  }
  return true;
};

const isUniqueAddress = (value, data) => {
  //get all of the wallet addresses provided
  const addresses = data.from[1].value.members.map((member) => member.address);
  //if addresses contains same string returns false
  return addresses.filter((address) => address === value).length === 1;
};

const addressValidator = Yup.string()
  .test('isValidAddress', 'Wallet address is invalid', isValidAddress)
  .test('isAddressUnique', 'Wallet address is not unique', isUniqueAddress)
  .required();

const inputsValidation = Yup.object({
  name: Yup.string().max(20).required(),
  members: Yup.array().when('restrictionType', {
    is: REALM_RESTRICTION_TYPE.STANDARD,
    then: Yup.array(
      Yup.object({
        index: Yup.number(),
        address: addressValidator
      })
    )
  }),
  nftAddresses: Yup.array().when('restrictionType', {
    is: REALM_RESTRICTION_TYPE.NFT,
    then: () =>
      Yup.array(
        Yup.object({
          chainId: Yup.number()
            .required('Chain id is required')
            .integer('Please input integer value')
            .positive('Please input positive value')
            .typeError('Please input number value'),
          address: Yup.string()
            .required('Address is required')
            .test('is-address-valid', 'Wrong address value', (value) =>
              validateContractAddress(value)
            ),
          minNftAmount: Yup.number()
            .typeError('Please input number value')
            .integer('Please input integer value')
            .min(1, 'Minimum NFT amount is 1')
        })
      )
  }),
  strategies: Yup.array()
    .min(1, 'strategies is a required field')
    .required('strategies is a required field'),
  width: Yup.number().min(80).max(150),
  height: Yup.number().min(80).max(150)
});

const networksArr = Object.values(networks);

const sendRequest = async (isEdit, data, address) => {
  const result = isEdit
    ? await axios.put(`/api/spaces?address=${address}`, data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
    : await axios.post(`/api/spaces?address=${address}`, data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
  return result;
};

const getStrategiesToDisplay = (strategies) =>
  strategies.map((strategy) => {
    const nftAddresses = getAddressesToDisplay(strategy.params.nftAddresses);
    const addresses = getAddressesToDisplay(strategy.params.addresses);

    const additionalInfo =
      strategy.params.nftName ||
      strategy.params.address ||
      (nftAddresses?.length && nftAddresses.join(', ')) ||
      (addresses?.length && addresses.join(', '));

    return {
      ...strategy,
      displayValue: `${strategy.name} ${additionalInfo ? '(' + additionalInfo + ')' : ''}`
    };
  });

const getAddressesToDisplay = (addresses) =>
  addresses &&
  Object.keys(addresses)
    .filter((key) => !!addresses[key])
    .map((key) => addresses[key] && key + ': ' + addresses[key]);

const SpaceForm = ({ isEdit }) => {
  const router = useRouter();
  const [popStatus, setPopStatus] = useState(false);
  const [space] = useGlobalState('space');
  const [strategies] = useGlobalState('strategies');
  const [isGlobalAdmin] = useGlobalState('isGlobalAdmin');
  const [address] = useGlobalState('address');
  const [imageFile, setImageFile] = useState();
  const [logoFile, setLogoFile] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [fileError, setFileError] = useState();
  const [isStrategiesFieldTouched, setStrategiesFieldTouched] = useState(false);
  const [spaces, setSpaces] = useState([]);
  const [showModal, setShowModal] = useState({ status: false, text: '' });
  const [initialValues, setInitialValues] = useState({
    name: '',
    networks: [],
    strategies: [],
    members: [{ index: 0, address: '' }],
    nftAddresses: defaultNftAddresses,
    showMintingButton: false,
    mintingButtonCustomLink: '',
    height: '',
    width: '',
    hidden: false,
    restrictionType: REALM_RESTRICTION_TYPE.STANDARD,
    isNamespace: false,
    parentNamespace: ''
  });

  const strategiesToDisplay = useMemo(() => getStrategiesToDisplay(strategies), [strategies]);

  const getAllSpaces = async () => {
    try {
      let spacesData = await axios.get(`/api/spaces`);
      if (spacesData?.data) {
        const namespaces = spacesData.data.map((space) => {
          return {
            value: space._id,
            label: space.name,
            className: styles.option
          };
        });
        namespaces.unshift({
          value: null,
          label: 'None',
          className: styles.option
        });
        setSpaces(namespaces);
      }
    } catch (error) {
      console.log('Get all spaces error:' + error);
    }
  };

  useEffect(() => {
    (async () => {
      await getAllSpaces();
    })();
  }, []);

  const validateFile = (file) => {
    if (!file && isEdit) return false;
    if (!file) return 'Both logo and header logo files are required';

    const tooBig = file.size >= 1048576;
    return tooBig ? 'One of the files is too big, maximum size: 1MB.' : false;
  };
  const setValidateFile = (file, errorSetter, fileSetter) => {
    const error = validateFile(file);
    errorSetter(error);
    !error && fileSetter(file);
  };

  const onBlur = () => {
    if (!isStrategiesFieldTouched) setStrategiesFieldTouched(true);
  };

  const handleSubmit = async (values) => {
    try {
      setIsLoading(true);
      const nftAddressesData = values.nftAddresses.map((addr) => ({
        chainId: Number(addr.chainId),
        minNftAmount: Number(addr.minNftAmount),
        address: addr.address
      }));

      const spaceName = values.name.replaceAll('-', ' ');
      const spaceKey = isEdit ? space.key : spaceName.replace(' ', '').toLowerCase();

      const payload = {
        ...values,
        name: spaceName,
        members:
          values.restrictionType === REALM_RESTRICTION_TYPE.STANDARD
            ? values?.members?.map((member) => member.address)
            : undefined,
        nftAddresses:
          values.restrictionType === REALM_RESTRICTION_TYPE.NFT ? nftAddressesData : undefined,
        networks: values.networks.map((network) => network.chainId),
        strategies: values.strategies.map((strategy) => strategy._id),
        key: spaceKey,
        parentNamespace: values.parentNamespace.value
      };

      const msg = JSON.stringify(payload);

      const formData = new FormData();

      imageFile && formData.append('image', imageFile);
      logoFile && formData.append('logo', logoFile);

      const data = {
        payload,
        msg,
        address
      };

      formData.append('data', JSON.stringify(data));

      const result = await sendRequest(isEdit, formData, address);

      if (result?.status === 200) {
        toast.success(
          isEdit
            ? `You have successfully updated realm ${space.name}`
            : `You have successfully added a new realm`,
          {
            position: 'top-center'
          }
        );
        router.push('/');
      }
    } catch (e) {
      toast.error(e, { position: 'top-center' });
      console.error(e);
    } finally {
      setIsLoading(false);
      setInitialValues(values);
    }
  };

  useEffect(() => {
    if (isEdit) {
      const networksArray = [];
      space.networks?.forEach((network) => {
        const networkObj = networksArr.find((networkData) => network === networkData.chainId);
        networksArray.push(networkObj);
      });

      const strategyArray = [];
      if (strategies.length) {
        space.strategies?.forEach((strategyId) => {
          const strategy = strategiesToDisplay.find((strategy) => strategy._id === strategyId);
          strategyArray.push(strategy);
        });
      }
      const members = space?.members?.map((member, index) => {
        return {
          index: index,
          address: member
        };
      });

      setInitialValues({
        name: space.name || '',
        networks: networksArray,
        strategies: strategyArray,
        members: members || initialValues.members,
        nftAddresses: space.nftAddresses?.length
          ? space.nftAddresses?.map((values, index) => ({
              itemId: index,
              errors: {},
              ...values
            }))
          : defaultNftAddresses,
        showMintingButton: space.showMintingButton,
        mintingButtonCustomLink: space.mintingButtonCustomLink,
        height: space.height || '',
        width: space.width || '',
        hidden: space.hidden,
        restrictionType: space.restrictionType || REALM_RESTRICTION_TYPE.STANDARD,
        isNamespace: space.isNamespace,
        parentNamespace: {
          value: space.parentNamespace?._id || null,
          label: space.parentNamespace?.name || 'None'
        }
      });
    } else
      setInitialValues({
        ...initialValues,
        parentNamespace: { value: space?._id || null, label: space?.name || 'None' }
      });
    setIsLoading(false);
  }, [strategies, space]);

  const sliderHandler = async ({ target: { checked } }) => {
    if (!isEdit || popStatus) return;
    setPopStatus(true);
    setShowModal({
      status: true,
      text: `If you change ${
        checked ? 'realm to a namespace' : 'namespace to a realm'
      }, all existing ${checked ? 'proposals' : 'realms'} inside will disappear`
    });
  };

  if (!isGlobalAdmin) {
    router.push('/');
    return null;
  }
  return !isLoading ? (
    <>
      <Dialog
        isOpen={showModal.status}
        handleClose={() => setShowModal((showModal.status = false))}>
        <div className={styles.modalTitle}>Caution!</div>
        <span>{showModal.text}</span>
      </Dialog>
      <Container>
        <Formik
          enableReinitialize={isEdit}
          initialValues={initialValues}
          validationSchema={inputsValidation}
          onSubmit={handleSubmit}>
          {({ values, errors, touched, setFieldValue }) => {
            return (
              <Form className={styles.box}>
                <TextInput
                  className={styles.input}
                  label="Name"
                  name="name"
                  type="text"
                  placeholder="Please enter realm name"
                  value={values.name}
                />
                <label className={styles.largeLabel} htmlFor="type">
                  Type
                  <Image
                    alt="info-icon"
                    src={infoIcon}
                    width={20}
                    height={20}
                    data-for="data-tip"
                    data-tip
                  />
                  <ReactTooltip place="right" type="info" id="data-tip" aria-haspopup="true">
                    <ul>
                      <li>
                        Wallet address based: With this type of realm you define who can create
                        proposals by providing a list of eligible wallet addresses
                      </li>
                      <li>
                        NFT based: With this type of realm you define who can create proposals by
                        providing a list of eligible token IDs of a specific NFT contract
                      </li>
                    </ul>
                  </ReactTooltip>
                </label>
                <Dropdown
                  id="restrictionType"
                  className={styles.dropdown}
                  controlClassName={styles.control}
                  placeholderClassName={styles.placeholder}
                  menuClassName={styles.menu}
                  options={REALM_RESTRICTION_OPTIONS}
                  value={values.restrictionType}
                  onChange={(type) => setFieldValue('restrictionType', type.value)}
                  arrowClosed={<span className={styles.arrowClosed} />}
                  arrowOpen={<span className={styles.arrowOpen} />}
                />
                <Select
                  name="networks"
                  label="Networks"
                  placeholder="Please select networks"
                  displayValue="name"
                  optionsArray={networksArr}
                  onChange={(networks) => setFieldValue('networks', networks)}
                  initialValue={values.networks}
                />
                <Select
                  name="strategies"
                  label="Strategies"
                  placeholder="Please select strategies"
                  value={values.strategies}
                  displayValue="displayValue"
                  optionsArray={strategiesToDisplay}
                  onChange={(list) => setFieldValue('strategies', list)}
                  initialValue={values.strategies}
                  error={errors.strategies}
                  touched={isStrategiesFieldTouched}
                  onBlur={onBlur}
                />
                <label className={styles.largeLabel} htmlFor="type">
                  Parent namespace
                </label>
                <Dropdown
                  id="parentNamespace"
                  className={styles.dropdown}
                  controlClassName={styles.control}
                  placeholderClassName={styles.placeholder}
                  menuClassName={styles.menu}
                  options={spaces}
                  value={values.parentNamespace}
                  onChange={(namespace) => {
                    setFieldValue('parentNamespace', namespace);
                  }}
                  arrowClosed={<span className={styles.arrowClosed} />}
                  arrowOpen={<span className={styles.arrowOpen} />}
                />
                <If condition={values.restrictionType === REALM_RESTRICTION_TYPE.STANDARD}>
                  <label className={styles.largeLabel} htmlFor="type">
                    Proposal creators
                  </label>
                  <SimpleInput
                    name="members"
                    setFieldValue={setFieldValue}
                    proposalCreators={values['members']}
                    elementsList="members"
                    errors={errors.members}
                  />
                </If>
                <If condition={values.restrictionType === REALM_RESTRICTION_TYPE.NFT}>
                  <label className={styles.largeLabel} htmlFor="type">
                    NFT Addresses
                  </label>
                  <Addresses
                    name="nftAddresses"
                    chainValues={values.nftAddresses}
                    setFieldValue={setFieldValue}
                    errors={errors.nftAddresses}
                    touched={touched}
                    buttonLabel="Add next chain"
                  />
                </If>
                <TextInput
                  className={styles.input}
                  label="Height"
                  name="height"
                  type="number"
                  placeholder="Input logo height here"
                />
                <TextInput
                  className={styles.input}
                  label="Width"
                  name="width"
                  type="number"
                  placeholder="Input logo width here"
                />
                <label className={styles.label}>
                  Upload image here:
                  <input
                    name="image"
                    className={styles.upload}
                    onChange={(e) => setValidateFile(e.target.files[0], setFileError, setImageFile)}
                    type="file"
                    placeholder="Insert your image here"
                  />
                </label>
                <label className={styles.label}>
                  Upload realm header here:
                  <input
                    name="logo"
                    className={styles.upload}
                    onChange={(e) => setValidateFile(e.target.files[0], setFileError, setLogoFile)}
                    type="file"
                    placeholder="Insert your logo here"
                  />
                </label>
                {fileError ? <p className={styles.error}>{fileError}</p> : null}
                <div className={styles.toggle}>
                  <TextInput
                    className={styles.switch}
                    name="showMintingButton"
                    type="checkbox"
                    label="Mintable"
                  />
                  <span className={styles.slider}></span>
                </div>
                {values.showMintingButton && (
                  <TextInput
                    className={styles.input}
                    label="Mint identity link"
                    name="mintingButtonCustomLink"
                    type="text"
                    placeholder="Please enter minting identity link"
                  />
                )}
                <div className={styles.toggle}>
                  <TextInput
                    className={styles.switch}
                    name="hidden"
                    type="checkbox"
                    label="Hidden?"
                  />
                  <span className={styles.slider}></span>
                </div>
                <div className={styles.toggle}>
                  <TextInput
                    className={styles.switch}
                    name="isNamespace"
                    type="checkbox"
                    label="Namespace"
                    onClick={sliderHandler}
                  />
                  <span className={styles.slider}></span>
                </div>
                <Button
                  onClick={onBlur}
                  className={styles.button}
                  type="submit"
                  disabled={fileError}>
                  {isEdit ? 'Submit changes' : 'Add new realm'}
                </Button>
              </Form>
            );
          }}
        </Formik>
      </Container>
    </>
  ) : (
    <PageLoading />
  );
};

export default SpaceForm;
