import axios from 'axios';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import TextArea from 'components/Ui/Formik/TextArea';
import TextInput from 'components/Ui/Formik/TextInput';
import token from 'contracts/Token';
import { ethInstance } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import { useGlobalState } from 'globalStateStore';
import { getBalanceOf } from 'helpers/influence';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import * as Yup from 'yup';
import styles from './CommunityForm.module.scss';

const inputsValidation = Yup.object({
  tokenName: Yup.string().max(160, 'Max 160 characters').required('Required'),
  tokenSymbol: Yup.string().max(255, 'Max 255 characters').required('Required'),
  tokenURL: Yup.string().max(255, 'Max 255 characters').required('Required'),
  description: Yup.string().max(255, 'Max 255 characters').required('Required'),
  proposalKind: Yup.string().max(255, 'Max 255 characters').required('Required')
});

const Submit = ({ canVote, validNetwork }) => {
  const [text, setText] = useState('Submit');
  const [disabled, setDisabled] = useState(false);
  useEffect(() => {
    setText('Submit');
    setDisabled(false);
    if (!canVote) {
      setText('Not enough cash');
      setDisabled(true);
    } else if (!validNetwork) {
      setText('Network not valid');
      setDisabled(true);
    }
  }, [canVote, validNetwork]);
  return (
    <Button disabled={disabled} className={styles.button} type="submit">
      {text}
    </Button>
  );
};

const CommunityForm = () => {
  const navigate = useNavigate();
  const [currentAddress] = useGlobalState('address');
  const [currentNetwork] = useGlobalState('currentNetwork');
  const [space] = useGlobalState('space');
  const validNetwork = token.networks[currentNetwork] ? true : false;
  const [canVote, setCanVote] = useState(false);
  const [address, setAddress] = useState(null);
  const [error, setError] = useState();

  useEffect(() => {
    (async () => {
      try {
        const address = await ethInstance.getEthAccount();
        const contract = await ethInstance.getContract('read', token);
        setAddress(address);
        const userBalance = await getBalanceOf(address, contract);
        userBalance > 0 ? setCanVote(true) : setCanVote(false);
      } catch (e) {
        setError(e.message);
        console.error(e);
      }
    })();
  }, [currentAddress]);

  const submit = async (values, { setSubmitting, resetForm }) => {
    try {
      const val = { ...values, space: space.key };
      const msg = JSON.stringify(val);

      resetForm();

      const data = {
        msg,
        address
      };

      if (canVote && validNetwork && space?.key) {
        const res = await axios.post('/api/suggestion', { data, address });

        if (res?.status === 200) {
          toast.success('Thank you for your suggestion!', { position: 'top-center' });
          navigate('/', { query: { prevPath: `/:${space?.key}/form` } });
        }
      } else {
        console.log("Request hasn't been sent");
        console.log('canVote', canVote);
        console.log('validNetwork', validNetwork);
        console.log('space-key', space?.key);
      }
    } catch (e) {
      setError(e.message);
      console.error(e);
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <Container>
      <h1 className={styles.title}>What token should be next?</h1>
      <Formik
        initialValues={{
          tokenName: '',
          tokenSymbol: '',
          tokenURL: '',
          description: '',
          proposalKind: ''
        }}
        validationSchema={inputsValidation}
        onSubmit={submit}>
        <Form className={styles.box}>
          <TextInput
            className={styles.input}
            label="Token Name"
            name="tokenName"
            type="text"
            placeholder="Please enter token name"
          />
          <TextInput
            className={styles.input}
            label="Token Symbol"
            name="tokenSymbol"
            type="text"
            placeholder="Please enter token symbol"
          />
          <TextInput
            className={styles.input}
            label="Token Url"
            name="tokenURL"
            type="url"
            placeholder="Please provide token URL"
          />
          <TextArea
            className={styles.textarea}
            label="Why this token?"
            name="description"
            type="text"
            placeholder="Please let us know, why would you like this token to appear in the next proposal..."
          />
          <TextInput
            className={styles.input}
            label="What kind of proposal does it regard?"
            name="proposalKind"
            type="text"
            placeholder="Please specify kind of proposal"
          />
          {error && <div className={styles.catch}>{error}</div>}
          <Submit canVote={canVote} validNetwork={validNetwork} />
        </Form>
      </Formik>
    </Container>
  );
};

export default CommunityForm;
