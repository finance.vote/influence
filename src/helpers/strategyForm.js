export const strategiesToShift = [
  'fvt-nft',
  'fvt-nft-binary-voting',
  'fvt-nft-sum',
  'fvt-nft-sum-binary'
];
export const strategiesToModify = {
  nft: 'nft-united',
  'nft-binary-voting': 'nft-binary-voting-united',
  'nft-sum': 'nft-sum-united',
  'nft-sum-binary': 'nft-sum-binary-united'
};
//TODO check on production which types of strategies are no longer used
export const userFriendlyStrategyLabels = {
  'token-weighted-chains-sum': 'Token Weighted',
  nft: 'Quadratic Voting',
  'nft-binary-voting': 'Binary Voting ',
  'nft-sum': 'Aggregated Quadratic Voting',
  'nft-sum-binary': 'Aggregated Binary Voting',
  'deposit-balance': 'Stake Weighted',
  'deposit-rank': 'Fibonacci Weighted ',
  'ethereum-vote-power': 'Ethereum based'
};

export function buildChainIdToAddressMap(addresses, stringify) {
  const map = addresses.reduce((acc, addr) => {
    acc[addr.chainId] = addr.address;
    return acc;
  }, {});
  return stringify ? JSON.stringify(map) : map;
}
