import axios from 'axios';
import Identity from 'contracts/Identity.json';
import Trollbox from 'contracts/Trollbox.json';
import VotingIdentity from 'contracts/VotingIdentity2.json';
import { ethInstance, ipfsGet, sha3 } from 'evm-chain-scripts';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';

const transferEventSignature = sha3('Transfer(address,address,uint256)');

export async function getMarketsVoiceCredits(identityId, tournamentId = 1) {
  try {
    if (!identityId) return 0;
    const trollbox = await ethInstance.getContract('read', Trollbox);
    const credits = await trollbox.getVoiceCredits(tournamentId, identityId);
    return credits;
  } catch (err) {
    console.error(`Unable to getVoiceCredits for identity ${identityId}`, err);
    return 0;
  }
}

export async function hasVoterId(address, nftAddresses) {
  let contract;
  if (nftAddresses) {
    const currentNetwork = await ethInstance.getChainId();
    if (!nftAddresses[currentNetwork]) return false;
    contract = await ethInstance.getReadContractByAddress(
      VotingIdentity,
      nftAddresses[currentNetwork]
    );
  } else {
    contract = await ethInstance.getContract('read', VotingIdentity);
  }
  const balance = await contract.balanceOf(address);
  return balance > 0;
}

export async function hasOS1155VoterId(address, collectionName) {
  const currentNetwork = await ethInstance.getChainId();
  const {
    data: { identities: myIdentities }
  } = await axios.get(`/api/identities/${currentNetwork}/${collectionName}/${address}`);
  return myIdentities.length > 0;
}

export function getNftContracts(nftAddresses) {
  let contracts = [];

  const abi = [
    {
      constant: true,
      inputs: [
        {
          internalType: 'address',
          name: 'account',
          type: 'address'
        }
      ],
      name: 'balanceOf',
      outputs: [
        {
          internalType: 'uint256',
          name: '',
          type: 'uint256'
        }
      ],
      payable: false,
      stateMutability: 'view',
      type: 'function'
    }
  ];

  if (nftAddresses) {
    for (const { chainId, address } of nftAddresses) {
      contracts.push(ethInstance.getReadContractByAddress({ abi }, address, chainId));
    }
  }

  return contracts;
}

export async function isNftMember(nftAddresses, walletAddress) {
  try {
    const contracts = await Promise.all(getNftContracts(nftAddresses));
    const balances = await Promise.all(
      contracts.map(async (contract) => {
        const balance = await contract.balanceOf(walletAddress);
        const settings = nftAddresses.find((nftItem) => nftItem.address === contract.address);

        return {
          balance,
          permission: balance >= settings.minNftAmount
        };
      })
    );
    return balances.some((contract) => contract.permission);
  } catch (err) {
    console.log('Error while checking nft members', err);
  }
}

export function getContractsByNFTAddresses(nftAddresses) {
  let contracts = [];

  if (nftAddresses) {
    for (const key in nftAddresses) {
      const addresses = nftAddresses[key];
      for (const nftAddress of addresses) {
        contracts.push(ethInstance.getReadContractByAddress(VotingIdentity, nftAddress, key));
      }
    }
  }

  return contracts;
}

export async function getCombinedVoterIds(address, nftAddresses, settings) {
  let contracts = getContractsByNFTAddresses(nftAddresses);

  const contractsResult = await Promise.all(contracts);
  const groupedNFTs = {};

  for (const contract of contractsResult) {
    const [symbol, balance] = await Promise.all([contract.symbol(), contract.balanceOf(address)]);
    const nftCounts = parseInt(balance);
    if (nftCounts == 0) continue;

    const isOldContract =
      settings.find((setting) => setting.address === contract.address).isOldImplementation ?? false;

    if (isOldContract) {
      const {
        data: { identities }
      } = await axios.get(`/api/identities/sim-multi/${contract.address}/${address}`);

      if (identities.length)
        groupedNFTs[contract.address] = identities.map((id) => {
          return { nftId: id, symbol: symbol };
        });
    } else {
      let nftIdsPromisses = [];

      for (let i = 0; i < nftCounts; i++) {
        nftIdsPromisses.push(contract.tokenOfOwnerByIndex(address, i));
      }

      const nftIds = await Promise.all(nftIdsPromisses);

      groupedNFTs[contract.address] = nftIds.map((id) => {
        return { nftId: parseInt(id), symbol: symbol };
      });
    }
  }

  return groupedNFTs;
}

export async function getVoterIds(address, nftAddresses) {
  try {
    let contract;
    if (nftAddresses) {
      const currentNetwork = ethInstance.getChainId();
      if (!nftAddresses[currentNetwork]) return false;
      contract = await ethInstance.getReadContractByAddress(
        VotingIdentity,
        nftAddresses[currentNetwork]
      );
    } else {
      contract = await ethInstance.getContract('read', VotingIdentity);
    }
    const balance = await contract.balanceOf(address);
    if (balance == 0) return [];
    const promises = [];
    for (let i = 0; i < balance; i++) {
      promises.push(contract.tokenOfOwnerByIndex(address, i));
    }
    const tokens = await Promise.all(promises);
    return tokens.reduce((result, token) => {
      result[token] = token;
      return result;
    }, {});
  } catch (err) {
    console.error(err);
  }
}

export async function getVoterId(address) {
  const contract = await ethInstance.getContract('read', VotingIdentity);
  // this throws if address has no tokens, assume the token we're interested in is at index 1
  const token = await contract.tokenOfOwnerByIndex(address, 0);
  return token;
}

export async function hasIdentityVoterId(address) {
  const contract = await ethInstance.getContract('read', Identity);
  const balance = await contract.balanceOf(address);
  return balance > 0;
}

export async function getIdentitiesForAccount(callback) {
  const account = await ethInstance.getEthAccount();
  const filter = { to: account };
  const contract = await ethInstance.getContract('read', Identity);
  const balance = await contract.balanceOf(account);
  if (balance === 0) return;
  const allEvents = await ethInstance.getAllEvents(
    async (event) => {
      const owner = await contract.ownerOf(event.tokenId);
      if (event.tokenId && owner.toLowerCase() === account.toLowerCase()) {
        event.tokenId = parseInt(event.tokenId);
        callback(event);
      } else {
        console.log('owner', owner, 'account', account);
      }
    },
    Identity,
    'Transfer',
    filter,
    0,
    'latest',
    transferEventSignature,
    Identity.abi[10].inputs
  );
  for (let event of allEvents) {
    const owner = await contract.ownerOf(event.tokenId);
    if (event.tokenId && owner.toLowerCase() === account.toLowerCase()) {
      event.tokenId = parseInt(event.tokenId);
      callback(event);
    } else {
      console.log('owner', owner, 'account', account);
    }
  }
}

export async function getAllIdentitiesForAccount(address, network, collectionName) {
  const {
    data: { identities }
  } = await axios.get(`/api/identities/${network}/${collectionName}/${address}`);
  return identities;
}

export async function getFromBackend(ipfsHash) {
  const url = `/api/proposals/${ipfsHash}`;
  const response = await axios.get(url);
  return response.data;
}

export function shortenWalletAddress(address) {
  return address ? `${address.substr(0, 6)}...${address.substr(-4)}` : '';
}

export async function getBalanceOf(address, contract) {
  try {
    const balance = await contract.balanceOf(address);
    return balance / 1e18;
  } catch (err) {
    return 0;
  }
}

export async function getBlockNumber() {
  try {
    const block = await ethInstance.getBlock('latest');
    return parseInt(block.number);
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getBlockNumberPerNetwork(network) {
  try {
    const block = await ethInstance.readWeb3(network)?.getBlock('latest');
    return block?.number ? parseInt(block.number) : null;
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function subgraphRequest(url, query, options = {}) {
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...options?.headers
    },
    body: JSON.stringify({ query: jsonToGraphQLQuery({ query }) })
  });
  const { data } = await res.json();
  return data || {};
}

export async function getNftMetadata(id, contractAddress, chainId = null) {
  if (id && id != 0 && contractAddress) {
    try {
      const identity = await ethInstance.getReadContractByAddress(
        VotingIdentity,
        contractAddress,
        chainId
      );
      const uri = await identity.tokenURI(parseInt(id));

      if (!uri) return null;
      let metadata;
      if (uri.startsWith('ipfs://')) metadata = await ipfsGet(uri.replace('ipfs://', ''));
      else if (uri.startsWith('Q')) metadata = await ipfsGet(uri);
      else {
        metadata = await fetch(uri).then((res) => res.json());
      }
      return metadata;
    } catch (e) {
      return null;
    }
  }
}

export default { getBlockNumber };
