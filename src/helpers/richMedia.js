export function validateYoutubeUrl(url) {
  var p =
    /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  if (url.match(p)) {
    return url.match(p)[1];
  }
  return false;
}

export function validateTwitterUrl(url) {
  var regex =
    /(?:http:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w-]*\/)*([\w-]*)/;
  if (url.match(regex)) {
    const splittedUrl = url?.split('/');
    return splittedUrl[splittedUrl.length - 1].split('?')[0];
  }
  return false;
}

export function validateXComUrl(url) {
  var regex = /(?:http:\/\/)?(?:www\.)?x\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w-]*\/)*([\w-]*)/;
  if (url.match(regex)) {
    const splittedUrl = url?.split('/');
    return splittedUrl[splittedUrl.length - 1].split('?')[0];
  }
  return false;
}

export function validateVimeoUrl(url) {
  // Look for a string with 'vimeo', then whatever, then a
  // forward slash and a group of digits.
  var match = /vimeo.*\/(\d+)/i.exec(url);

  // If the match isn't null (i.e. it matched)
  if (match) {
    // The grouped/matched digits from the regex
    return match[1];
  }
  return false;
}

export function validateTiktokUrl(url) {
  const regex = /(?:https:\/\/)?(?:m\.|www\.)?tiktok\.com\//;
  if (url.match(regex)) {
    return url;
  }
  return false;
}

export function validateAnchorUrl(url) {
  const regex = /(?:https:\/\/)?(?:m\.|www\.)?anchor\.fm\//;
  if (url.match(regex) && !url.includes('<iframe')) {
    return url;
  }
  return false;
}

export function validateMediaUrl(type, url) {
  switch (type) {
    case 'youtube':
      return validateYoutubeUrl(url);
    case 'vimeo':
      return validateVimeoUrl(url);
    case 'twitter':
      return validateTwitterUrl(url);
    case 'x.com':
      return validateXComUrl(url);
    case 'tiktok':
      return validateTiktokUrl(url);
    case 'image':
      return url;
    case 'anchor':
      return validateAnchorUrl(url);
    default:
      return false;
  }
}
