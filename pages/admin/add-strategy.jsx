import StrategyFormPage from 'pages/admin/StrategyForm';

const StrategyForm = () => {
  return <StrategyFormPage />;
};

export default StrategyForm;
