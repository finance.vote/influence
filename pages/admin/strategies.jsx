import StrategyListPage from 'pages/admin/StrategyList';

const StrategyList = () => {
  return <StrategyListPage />;
};

export default StrategyList;
